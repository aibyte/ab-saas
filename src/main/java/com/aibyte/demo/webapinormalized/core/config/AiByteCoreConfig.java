package com.aibyte.demo.webapinormalized.core.config;

import com.aibyte.demo.webapinormalized.core.env.EnvConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AiByteCoreConfig {

  @Value("${spring.profiles.active:NA}")
  private String activeProfile;

  @Bean
  public EnvConfig envConfig() {
    return EnvConfig.getEnvConfig(activeProfile);
  }

}
