package com.aibyte.demo.webapinormalized.core.env;

public class EnvConstant {

  public static final String ENV_DEV = "dev";
  public static final String ENV_TEST = "test";
  public static final String ENV_STG = "stg";
  public static final String ENV_PROD = "prod";
}
