package com.aibyte.demo.webapinormalized.core.env;

import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Builder
public class EnvConfig {

  private String name;
  private boolean debug;
  private String scheme;


  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private static Map<String, EnvConfig> map;

  static {
    map = new HashMap<>();
    EnvConfig envConfig = EnvConfig.builder().name(EnvConstant.ENV_DEV)
        .debug(true)
        .scheme("http")
        .build();
    map.put(EnvConstant.ENV_DEV, envConfig);

    envConfig = EnvConfig.builder().name(EnvConstant.ENV_TEST)
        .debug(true)
        .scheme("http")
        .build();
    map.put(EnvConstant.ENV_TEST, envConfig);

    // For aliCloud k8s demo, enable debug and use http and AB-uat.local
    // in real world, disable debug and use http and AB-uat.xyz in UAT environment
    envConfig = EnvConfig.builder().name(EnvConstant.ENV_STG)
        .debug(true)
        .scheme("http")
        .build();
    map.put(EnvConstant.ENV_STG, envConfig);

    envConfig = EnvConfig.builder().name(EnvConstant.ENV_PROD)
        .debug(false)
        .scheme("https")
        .build();
    map.put(EnvConstant.ENV_PROD, envConfig);
  }

  /**
   * Get current Environment config, default dev environment.
   *
   * @param env environment string
   * @return EnvConfig
   */
  public static EnvConfig getEnvConfig(String env) {
    EnvConfig envConfig = map.get(env);
    if (envConfig == null) {
      envConfig = map.get(EnvConstant.ENV_DEV);
    }
    return envConfig;
  }
}
