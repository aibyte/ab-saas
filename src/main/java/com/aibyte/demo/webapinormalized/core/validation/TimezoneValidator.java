package com.aibyte.demo.webapinormalized.core.validation;

import java.util.Arrays;
import java.util.TimeZone;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *  Timezone validator
 */
public class TimezoneValidator implements ConstraintValidator<Timezone, String> {

  @Override
  public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
    if (s == null) {
      return true;
    }
    return Arrays.asList(TimeZone.getAvailableIDs()).contains(s);
  }
}
