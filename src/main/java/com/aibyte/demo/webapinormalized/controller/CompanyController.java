package com.aibyte.demo.webapinormalized.controller;

import com.aibyte.demo.webapinormalized.core.validation.GroupValidationCreate;
import com.aibyte.demo.webapinormalized.dto.CompanyDto;
import com.aibyte.demo.webapinormalized.dto.GenericCompanyResponse;
import com.aibyte.demo.webapinormalized.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/company")
@Validated
public class CompanyController {

  @Autowired
  private CompanyService companyService;


  @PostMapping(path = "/create")
  public GenericCompanyResponse createCompany(
      @RequestBody @Validated({GroupValidationCreate.class}) CompanyDto companyDto) {
    CompanyDto newCompanyDto = companyService.createCompany(companyDto);
    return new GenericCompanyResponse(newCompanyDto);
  }

  @GetMapping(path = "/get")
  public GenericCompanyResponse getCompany(@RequestParam("company_id") String companyId) {
    CompanyDto companyDto = companyService.getCompany(companyId);
    return new GenericCompanyResponse(companyDto);
  }

}
