package com.aibyte.demo.webapinormalized.dto;

import com.aibyte.demo.webapinormalized.core.validation.GroupValidationCreate;
import com.aibyte.demo.webapinormalized.core.validation.GroupValidationUpdate;
import com.aibyte.demo.webapinormalized.core.validation.Timezone;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CompanyDto {

  @NotBlank(groups = {GroupValidationUpdate.class})
  private String id;
  @NotBlank(groups = {GroupValidationCreate.class, GroupValidationUpdate.class})
  private String name;
  private boolean archived;
  @Timezone(groups = {GroupValidationCreate.class, GroupValidationUpdate.class})
  @NotBlank(groups = {GroupValidationCreate.class, GroupValidationUpdate.class})
  private String defaultTimezone;
  @NotBlank(groups = {GroupValidationCreate.class, GroupValidationUpdate.class})
  private String defaultDayWeekStarts;
}
