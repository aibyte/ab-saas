package com.aibyte.demo.webapinormalized.service;

import com.aibyte.demo.webapinormalized.core.error.ServiceException;
import com.aibyte.demo.webapinormalized.dto.CompanyDto;
import com.aibyte.demo.webapinormalized.core.api.ResultCode;
import com.aibyte.demo.webapinormalized.model.Company;
import com.aibyte.demo.webapinormalized.repo.CompanyRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

  @Autowired
  private CompanyRepo companyRepo;

  @Autowired
  private ModelMapper modelMapper;


  public CompanyDto createCompany(CompanyDto companyDto) {
    Company company = this.convertToModel(companyDto);

    Company savedCompany = null;
    try {
      savedCompany = companyRepo.save(company);
    } catch (Exception ex) {
      String errMsg = "could not create company";
//      serviceHelper.handleErrorAndThrowException(logger, ex, errMsg);
    }

//    LogEntry auditLog = LogEntry.builder()
//        .currentUserId(AuthContext.getUserId())
//        .authorization(AuthContext.getAuthz())
//        .targetType("company")
//        .targetId(company.getId())
//        .companyId(company.getId())
//        .teamId("")
//        .updatedContents(company.toString())
//        .build();

//    logger.info("created company", auditLog);

//    serviceHelper.trackEventAsync("company_created");

    return this.convertToDto(savedCompany);
  }

  public CompanyDto getCompany(String companyId) {

    Company company = companyRepo.findCompanyById(companyId);
    if (company == null) {
      throw new ServiceException(ResultCode.NOT_FOUND, "Company not found");
    }

    return this.convertToDto(company);

  }


  private CompanyDto convertToDto(Company company) {
    return modelMapper.map(company, CompanyDto.class);
  }

  private Company convertToModel(CompanyDto companyDto) {
    return modelMapper.map(companyDto, Company.class);
  }
}
