package com.aibyte.demo.webapinormalized;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebapiNormalizedApplication {

  public static void main(String[] args) {
    SpringApplication.run(WebapiNormalizedApplication.class, args);
  }

}
